import khipu

# START
if __name__ == '__main__':

    i2c_list = []
    for pendant in [[6,7],[10,11],[14,15]]:
        i2c_list.append(SoftI2C(sda=Pin(pendant[0]), scl=Pin(pendant[1]), freq=10000))

    output = {0: False, 1: False, 2: False}

    # only used to detect newly attached pendants
    last_ids_present = {0: [], 1: [], 2: []}

    while 1:        
    
        for index, i2c in enumerate(i2c_list):
            ids_present = []
            try: output[index] = pendant.go(i2c, ids_present)
            except: output[index] = False            
            try: pendant.shake(i2c, last_ids_present[index], ids_present)
            except: pass
            last_ids_present[index] = ids_present

        print(output)
