# Copyright (C) 2024 Then Try This
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.f

from machine import I2C, Pin, RTC, SoftI2C
import utime as time
import khipu
import radio
import khipu_parse
import yarnc
import yarnasm

robot_radio = radio.radio()
robot_asm = yarnasm.assembler()

def ping():
    while 1:
        print("ping")
        robot_radio.send(robot_radio.build_halt())
        robot_radio.send(robot_radio.build_ping())
        time.sleep(1)

robot_knot_lookup = {
    0: "", # nop
    1: "-/-", # 1 short flash
    2: "-//-", # 2 long flash
    3: "-///-", # 3 stop walking
    4: "-////-", # 4 start walking forwards
    5: "-/////-", # 5 start walking backwards
    6: "-//////-", # 6 start turning left
    7: "-///////-", # 7 start turning right
    8: "-////////-", # 8 fast movements (increase??)
    9: "-/////////-", # 9 slow movements (decrease??)
}

if __name__ == '__main__':
    i2c_list = []
    output = {}
    last_ids_present = {}
    for i, pendant in enumerate([[10,11],[12,13],[14,15],
                                 [18,19],[20,21],[26,27]]):
        i2c_list.append(SoftI2C(sda=Pin(pendant[0]), scl=Pin(pendant[1]), freq=10000))
        output[i] = False
        last_ids_present[i] = []
        
    while 1:                
        for index, i2c in enumerate(i2c_list):
            ids_present = []
            try: output[index] = khipu.read_khipu(i2c, ids_present)
            except Exception as e:
                #print(e)
                output[index] = False            
            try:
                khipu.shake(i2c, last_ids_present[index], ids_present)
                pass
            except:
                pass

            if last_ids_present[index] != ids_present and output[index] != False:
                robot_radio.change_robot(index+1)
                seq = khipu_parse.pendant_to_sequence(output[index])
                print(seq)
                lisp = khipu_parse.sequence_to_code(seq, robot_knot_lookup)
                print(lisp)
                asm = yarnc.compile_program(lisp)
                print(asm)
                bytecode = robot_asm.assemble_bytes(asm)
                #print(bytecode)
                #robot_radio.send(robot_radio.build_write(3,bytearray([0])))
                #robot_radio.send(robot_radio.build_ping())
                robot_radio.send_code(bytecode)    

            last_ids_present[index] = ids_present
                

        time.sleep(0.1)
