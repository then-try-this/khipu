# Copyright (C) 2025 Then Try This
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.f

def pendant_to_sequence(container):
    ret = [0, 0, 0, 0]
    # insert ids into array
    for pendant in container:
        # are we a knot?
        if len(pendant['desc']) == 0:
            order = pendant['order'] 
            ret[order] = pendant['id']

    # now paste in subpendants
    pos = 0
    for pendant in container:
        # are we a knot?
        if len(pendant['desc']) > 0:
            pos += pendant['order']            
            ret = ret[:pos]+pendant_to_sequence(pendant['desc'])+ret[pos:]
    return ret

###########################################
# code (typed)
#
# -/- : short flash / wait
# -//- : long flash / wait

# -/-/- : stop walking
# -/-//- : start walking forwards
# -/-///- : start walking backwards

# -//-/- : start turning left
# -//-//- : start turning right

# -///-/- : fast movements (increase??)
# -///-//- : slow movements (decrease??)

############################################
# single knot (to match alex)
#
# -/- : short flash / wait
# -//- : long flash / wait

# -///- : stop walking
# -////- : start walking forwards
# -/////- : start walking backwards

# -//////- : start turning left
# -///////- : start turning right

# -///////- : fast movements (increase??)
# -////////- : slow movements (decrease??)

def walk_code(pattern):
    return [
        ["set!", "next_pattern", pattern],
        ["set!", "step_count_reset", 1],
        ["while", ["<", "step_count", 1], 0],
    ]

def knot_to_code(knot):
    if knot == "nop" or knot == "":
        return []
    # short wait
    if knot == "-/-": # 1
        return [
            ["set!", "led", 1],
            ["set!", "led", 0],
        ]
    # long wait
    if knot == "-//-": # 2
        return [
            ["set!", "led", 1],
            ["set!", "step_count_reset", 1],
            ["while", ["<", "step_count", 1], 0],
            ["set!", "led", 0],
        ]
    elif knot == "-///-": # 3
        return walk_code("walk_stop")
    elif knot == "-////-": # 4
        return walk_code("walk_forward")
    elif knot == "-/////-": # 5
        return walk_code("walk_backward")
    elif knot == "-//////-": # 6
        return walk_code("turn_left")
    elif knot == "-///////-": # 7
        return walk_code("turn_right")    
    elif knot == "-////////-": # 8
        return [["set!", "servo_ms_per_step", 500]]
    elif knot == "-/////////-": # 9
        return [["set!", "servo_ms_per_step", 250]]    
    else:
        print("no code for knot: "+knot)
    return []


def sequence_to_code(seq, knot_lookup):
    code = []
    for knot_id in seq:
        if knot_id in knot_lookup:
            knot = knot_lookup[knot_id]
            code = code + knot_to_code(knot)
        else:
            print("no knot for this ID: "+str(knot_id))
    return [
        #["defun", ["wait"],
        # ["set!", "usr_d", 5],
        # ["while", [">", "usr_d", 0], 
        #  ["set!", "usr_d", ["-", "usr_d", 1]]]],
        ["forever",
         # reset walk and turn off led at start
         ["set!", "next_pattern", "walk_stop"],
         ["set!", "led", 0]
         ] + code
    ]

if __name__ == '__main__':

    pend = {
        'id': 20,
        'order': 1,
        'desc': [
            {
                'id': 24,
                'order': 0,
                'desc': []
            },
            {
                'id': 25,
                'order': 2,
                'desc': [
                    {
                        'id': 30,
                        'order': 0,
                        'desc': []
                    }
                ]
            },
            {
                'id': 21,
                'order': 3,
                'desc': []
            }
        ]
    }
    
    knot_lookup = {
        0: "",
        21: "-/-",
        24: "-//-",
        30: "-///-"
    }

    seq = pendant_to_sequence([pend])
    print(sequence_to_code(seq, knot_lookup))
      
