import utime as time
from machine import I2C, Pin, RTC, SoftI2C
import struct

I2C_DEVICE = 16
CMD_PING         = 0x00
CMD_SET_RADIO_ID = 0x01
CMD_FILL_DATA    = 0x02
CMD_TRANSMIT     = 0x03

print("helloxxxx")

class radio:
    def __init__(self, i2c):
        print("radio init")
        self.i2c = i2c
        self.code_start = 64
        self.stop_osc=False
        self.telemetry=[]
                
    def ping(self):
        print("WOOWOWP")
    
    def request_telemetry(self,addr,start):
        return self.send(self.build_telemetry(start))
    
    def send_set(self,addr,address,val):
        self.send(self.build_write(address,struct.pack(b"H",val)))
    
    def send_calibrate(self,addr,do_cali,samples,sense):
        return self.send(self.build_calibrate(do_cali,samples,sense))

    def send_save_eeprom(self,addr):
        self.send(self.build_eewrite())
    
    def send_code(self, code):
        print("send code")
        l = len(code)
        self.send(self.build_halt())
        max_code_size = 32-6
        if l>max_code_size:
            # chop into packets
            pos=0
            pos16=0
            while pos<l:
                buf=""
                for i in range(0,max_code_size):
                    if pos<l:
                        buf+=code[pos]
                    else:
                        buf+='\0'
                    pos+=1

                print("writing chopped to "+str(self.code_start+pos16))
                self.send(self.build_write(self.code_start+pos16,buf)) 
                pos16+=max_code_size/2
                # make time for receipt?
        else:
            print("writing to "+str(self.code_start))
            self.send(self.build_write(self.code_start,code))
        # hmm, only checks the reset message, could be cleverer
        return self.send(self.build_reset())                

        
    def send_sync(self,addr,beat,ms_per_step,reg_sets):
        #if a_reg_set==1: print("starting: "+str(addr[4]))
        return self.send(self.build_sync(beat,ms_per_step,reg_sets))

    def send_pattern(self,addr,p,ms_per_step):
        self.send(self.build_pattern(p,ms_per_step))
    
    def write(self,b):
        try:
            self.i2c.writeto(I2C_DEVICE, b.to_bytes(1,"little"))
        except Exception as e:
            print("error writing to basestation i2c")
            print(e)
        time.sleep(0.01)

    def send(self,b):
        #print("sending "+b[0]+" to "+str(self.destination_address))
        if len(b)!=32:
            print("wrong number of bytes in message: "+str(len(b)))
            return False
        else:
            self.write(CMD_FILL_DATA)
            for byte in b:
                self.write(byte)
            self.write(CMD_TRANSMIT)
        time.sleep(0.1)

    def build_ping(self):
        return struct.pack("sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",b"P")

    def build_halt(self):
        return struct.pack("sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",b"H")

    def build_reset(self):
        return struct.pack("sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",b"R")

    def build_pattern(self,p,ms_per_step):
        return struct.pack("sxHH", 'M', ms_per_step, 4)+p+"00000000000000"
    
    def build_eewrite(self):
        return struct.pack("sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",b"E")

    def build_sync(self,beat,ms_per_step,reg_sets):
        regs=[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]
        
        for i in range(0,len(reg_sets)):
            regs[i]=reg_sets[i]

        # just sending two regs for now because arg
        return struct.pack("sxHHHHHHxxxxxxxxxxxxxxxxxx",b"S",beat,ms_per_step,regs[0][0],regs[0][1],regs[1][0],regs[1][1])

    def build_calibrate(self,do_cali,samples,return_sense):
        return struct.pack("sbHbxxxxxxxxxxxxxxxxxxxxxxxxxxx",b"C",do_cali,samples,return_sense)

    def build_telemetry(self,addr):
        return struct.pack("sxHxxxxxxxxxxxxxxxxxxxxxxxxxxxx",b"T",addr)  
        
    def build_write(self,addr,data):
        #print("sending "+str(len(data))+" bytes of data to "+str(addr))
        if len(data)>32-6:
            print("build_write: data is too large ("+str(len(data))+" bytes)")
        else:
            padding = 32-6-len(data)
            pad=b""
            for i in range(0,padding):
                pad+=b"\0"
            return struct.pack("sxHH",b"W",addr,len(data))+data+pad    




