import time
from machine import I2C, Pin, RTC, SoftI2C
import yarnc
import yarnasm
import radio

i2c = I2C(0, sda=Pin(16), scl=Pin(17), freq=10000)
r = radio.radio(i2c)

asm_code = yarnc.compile_program([
    ["defvar", "v", 0],
    ["set!", "next_pattern", 2],
    ["forever",
     ["set!", "v", ["+", "v", 1]],
     ["set!", "led", 1],
     ["set!", "led", 0]]
])

asm = yarnasm.assembler()

code = asm.assemble_bytes(asm_code)

flop = 1

r.write(radio.CMD_SET_RADIO_ID)
r.write(8)

while True:
    #time.sleep(3)
    #r.send(r.build_ping())
    r.send(r.build_write(3,bytes([flop])))
    #r.send(r.build_ping())
    #r.send(r.build_eewrite())

    if flop==1:
        flop=0
    else:
        flop=1
    
    r.send_code(code)    

    
    time.sleep(10.0)
