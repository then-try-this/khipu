import usys
import ustruct as struct
import utime
from machine import Pin, SPI
from nrf24l01 import *
from micropython import const

class radio:
    def __init__(self):
        # Addresses are in little-endian format.
        pipes = (b"\x08\xa7\xa7\xa7\xa7",b"\xaa\xa7\xa7\xa7\xa7")
        cfg = {"spi": 0, "miso": 4, "mosi": 7, "sck": 6, "csn": 8, "ce": 17}
        csn = Pin(cfg["csn"], mode=Pin.OUT, value=1)
        ce = Pin(cfg["ce"], mode=Pin.OUT, value=0)
        spi = SPI(
            cfg['spi'],
            baudrate=400000,
            sck=Pin(cfg["sck"]),
            mosi=Pin(cfg["mosi"]),
            miso=Pin(cfg["miso"])
        )
        self.nrf = NRF24L01(spi, csn, ce, channel=100, payload_size=32)
        self.nrf.open_tx_pipe(pipes[0])
        self.nrf.open_rx_pipe(1, pipes[1])
        self.nrf.start_listening()
        
        print("radio init")
        self.dump()
        
        self.code_start = 64
        self.stop_osc=False
        self.telemetry=[]
        self.station_id = b"\xa7\xa7\xa7\xa7"

    def print_byte_register(self, name, reg, qty=1):
        str = ""
        extra_tab = '\t' if len(name) < 8 else 0
        str+="%s\t%c =" % (name, extra_tab)
        while qty > 0:
            str+="0x%02x " % (self.nrf.reg_read(reg))
            qty -= 1
            reg += 1
        print (str)

    def print_address_register(self, name, reg, qty=1):
        extra_tab = '\t' if len(name) < 8 else 0
        str = "%s\t%c =" % (name, extra_tab)
        buf = []
        while qty > 0:
            qty -= 1
            for i in range(0, 5):
                buf.append(self.nrf.reg_read(reg+i))
            reg += 1
            str+=" 0x"
            for i in reversed(buf):
                str+="%02x" % i

        print (str)
        
    def dump(self):
        #self.print_status(self.get_status())
        self.print_address_register("RX_ADDR_P0-1", RX_ADDR_P0, 2)
        self.print_byte_register("RX_ADDR_P2-5", RX_ADDR_P2, 4)
        self.print_address_register("TX_ADDR", TX_ADDR)
        self.print_byte_register("RX_PW_P0-6", RX_PW_P0, 6)
        self.print_byte_register("EN_AA", EN_AA)
        self.print_byte_register("EN_RXADDR", EN_RXADDR)
        self.print_byte_register("RF_CH", RF_CH)
        self.print_byte_register("RF_SETUP", RF_SETUP)
        self.print_byte_register("CONFIG", CONFIG)
        self.print_byte_register("DYNPD/FEATURE", DYNPD, 2)

    def change_robot(robot_id):
        self.nrf.open_tx_pipe(int(robot_id).to_bytes() + self.station_id)
        
    def request_telemetry(self,addr,start):
        return self.send(self.build_telemetry(start))
    
    def send_set(self,addr,address,val):
        self.send(self.build_write(address,struct.pack(b"H",val)))
    
    def send_calibrate(self,addr,do_cali,samples,sense):
        return self.send(self.build_calibrate(do_cali,samples,sense))
    
    def send_save_eeprom(self,addr):
        self.send(self.build_eewrite())
    
    def send_code(self, code):
        l = len(code)
        self.send(self.build_halt())
        max_code_size = 32-6
        if l>max_code_size:
            # chop into packets
            pos = 0
            pos16 = 0
            while pos<l:
                buf = bytearray(max_code_size)
                for i in range(0,max_code_size):
                    if pos<l:
                        buf[i]=code[pos]
                    pos+=1
                self.send(self.build_write(self.code_start+pos16,buf)) 
                pos16 += int(max_code_size / 2)
                # make time for receipt?
        else:
            print("writing to "+str(self.code_start))
            self.send(self.build_write(self.code_start,code))
        # hmm, only checks the reset message, could be cleverer
        return self.send(self.build_reset())                

        
    def send_sync(self,addr,beat,ms_per_step,reg_sets):
        #if a_reg_set==1: print("starting: "+str(addr[4]))
        return self.send(self.build_sync(beat,ms_per_step,reg_sets))

    def send_pattern(self,addr,p,ms_per_step):
        self.send(self.build_pattern(p,ms_per_step))
    
    def send(self,b):
        #print("sending "+str(chr(b[0])))
        #print(b)
        if len(b)!=32:
            print("wrong number of bytes in message: "+str(len(b)))
            return False
        else:
            self.nrf.stop_listening()
            try:
                self.nrf.send(b)
            except OSError as e:
                pass #print(e)
            self.nrf.start_listening()
            utime.sleep(0.01)
        
    def build_ping(self):
        return struct.pack("sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",b"P")

    def build_halt(self):
        return struct.pack("sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",b"H")

    def build_reset(self):
        return struct.pack("sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",b"R")

    def build_pattern(self,p,ms_per_step):
        return struct.pack("sxHH", 'M', ms_per_step, 4)+p+"00000000000000"
    
    def build_eewrite(self):
        return struct.pack("sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",b"E")

    def build_sync(self,beat,ms_per_step,reg_sets):
        regs=[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]
        
        for i in range(0,len(reg_sets)):
            regs[i]=reg_sets[i]

        # just sending two regs for now because arg
        return struct.pack("sxHHHHHHxxxxxxxxxxxxxxxxxx",b"S",beat,ms_per_step,regs[0][0],regs[0][1],regs[1][0],regs[1][1])

    def build_calibrate(self,do_cali,samples,return_sense):
        return struct.pack("sbHbxxxxxxxxxxxxxxxxxxxxxxxxxxx",b"C",do_cali,samples,return_sense)

    def build_telemetry(self,addr):
        return struct.pack("sxHxxxxxxxxxxxxxxxxxxxxxxxxxxxx",b"T",addr)  
        
    def build_write(self,addr,data):
        #print("sending "+str(len(data))+" bytes of data to "+str(addr))
        if len(data)>32-6:
            print("build_write: data is too large ("+str(len(data))+" bytes)")
        else:
            padding = 32-6-len(data)
            pad=b""
            for i in range(0,padding):
                pad+=b"\0"
            return struct.pack("sxHH",b"W",addr,len(data))+data+pad    




