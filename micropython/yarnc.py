
reg_table = {
    "pc": "0000",
    "stack": "0001",
    "robot_id": "0002",
    "led": "0003",
    "comp_angle": "0004",
    "comp_delta_reset": "0005",
    "comp_delta": "0006",
    "step_count": "0007",
    "step_count_reset": "0008",
    "next_pattern": "0009",
    "ready_semaphore": "0010",
    "usr_a": "0010",
    "usr_b": "0011",
    "usr_c": "0012",
    "usr_d": "0013",
    
    # robot regs
    "servo_ms_per_step": "0014",
    "servo_1_amp": "0015",
    "servo_2_amp": "0016",
    "servo_3_amp": "0017",
    "servo_1_bias": "0018",
    "servo_2_bias": "0019",
    "servo_3_bias": "0020",
    "servo_1_smooth": "0021",
    "servo_2_smooth": "0022",
    "servo_3_smooth": "0023",

    "temperature": "0024",
    "sleep": "0025",
    "i2c_device": "0026",
    "i2c_addr": "0027",
    "i2c_data": "0028",
    "i2c_ctrl": "0029",
    "vcc": "0030",

    "accel_x": "0031",
    "accel_y": "0032",
    "accel_z": "0033",
    "gyro_x": "0034",
    "gyro_y": "0035",
    "gyro_z": "0036",
    "comp_x": "0037",
    "comp_y": "0038",
    "comp_z": "0039",

    "neo_red": "0040",
    "neo_green": "0041",
    "neo_blue": "0042",
    "neo_index": "0043",

    "reg_pattern_start": "0044",
    "reg_pattern_front_1": "0044",  # 16bit
    "reg_pattern_front_2": "0045",
    "reg_pattern_middle_1": "0046",
    "reg_pattern_middle_2": "0047",
    "reg_pattern_back_1": "0048",
    "reg_pattern_back_2": "0049",
    "reg_pattern_end": "0050",
    
    "reg_usr_start": "0056", 
    "reg_usr_end": "0064",
    "reg_code_start": "0064",
}

# internal compiler register on zero page
working_reg = "53"
stack_frame = "54"
var_start = 55

# constants lookup
constants_table = {
    "walk_null": "0",
    "walk_stop": "1",
    "walk_forward": "2",
    "walk_backward": "3",
    "turn_left": "4",
    "turn_right": "5",
    "turn_left2": "6",
    "turn_right2": "7",
    "walk_silly": "8",

    "i2c_read": "1",
    "i2c_write": "2",

    # see also linefollower.c
    "linefollower_i2c": "10",
    "linefollower_id": "0",
    "linefollower_i2c": "1", 
    "linefollower_alive": "2",
    "linefollower_exposure_lo": "3",
    "linefollower_exposure_hi": "4",
    "linefollower_courseness": "5",
    "linefollower_min_width": "6",
    "linefollower_max_width": "7",
    "linefollower_average": "8",
    "linefollower_line_midpos": "9",
    "linefollower_barcode_bits": "10",
    "linefollower_barcode_skip": "11",
    "linefollower_barcode_data": "12",
    "linefollower_line_error": "13",
    "linefollower_barcode_error": "14",
    "linefollower_do_autoexposure": "15",
    "linefollower_num_features": "31",
    "linefollower_features_start": "322",
    "linefollower_raw_image_start": "127",
}

constants = constants_table
label_id = 0
variables = []
histogram = []

def reg_table_lookup(x):
    return reg_table.get(x, False)

#-------------------------------------------------------------
# a label generator

label_id = 99

def generate_label(name):
    global label_id
    label_id += 1
    return f"{name}_{label_id}"

#----------------------------------------------------------------
# variables are just an address lookup table to var_start

variables = []

def make_variable(name):
    if name not in variables:
        variables.append(name)

def variable_lookup(name):
    def _lookup(l, c):
        if not l:
            print(f"yarnc ERROR: cant find variable {name}")
            return False
        elif name == l[0]:
            return str(c + var_start)
        else:
            return _lookup(l[1:], c + 1)
    
    return _lookup(variables, 0)

#----------------------------------------------------------------

def make_constant(name, value):
    if not isinstance(value, str):
        print(f"yarnc ERROR: constant {name} is not a string: {value}")
    constants.append([name, value])

def constant_lookup(name):
    if not name in constants:
        return False
    else:
        return constants[name]

#------------------------------------------------------------------
# store function args here - this is not a proper call stack
# (considered far too bloaty!), so these get clobbered with function
# calls within function calls - beware...

fnargs_start = 0x1

def fnarg(index):
    return str(fnargs_start + index)

fnarg_mapping = []

def set_fnarg_mapping(args):
    global fnarg_mapping
    fnarg_mapping = [[arg, fnarg(len(fnarg_mapping))] for arg in args]

def clear_fnarg_mapping():
    global fnarg_mapping
    fnarg_mapping = []

def fnarg_lookup(name):
    for arg in fnarg_mapping:
        if arg[0] == name:
            return arg[1]
    return False

# -----------------------

def is_fnarg(name):
    return any(arg[0] == name for arg in fnarg_mapping)

def emit_load_fnarg(name):
    return (
        emit("ld", stack_frame, f"\t ;; loading fnarg {name} ") +
        emit("addl", fnarg_lookup(name)) +
        emit("ldsi")  # load from top of stack indirect
    )

#----------------------------------------------------------------

# lookup a symbol everywhere, in order...
def lookup(name):
    # check registers first
    reg = reg_table_lookup(name)
    if reg:
        return reg
    else:
        # then constants
        print("looking up "+name)
        const = constant_lookup(name)
        if const:
            return const
        else:
            # finally variables
            return variable_lookup(name)

#----------------------------------------------------------------

def dash_to_underscore(s):
    return ''.join('_' if c == '-' else c for c in s)

#---------------------------------------------------------------

def immediate_value(x):
    if isinstance(x, (int, float)):
        return str(x)
    lu = lookup(x)
    if lu:
        return f"{lu}" #\t\t;; {x}"
    return str(x)

# is this an immediate value
def immediate(x):
    return isinstance(x, (int, float)) or isinstance(x, str)

# is this a primitive call?
def primcall(x):
    return isinstance(x, list) and len(x) > 0 and isinstance(x[0], str)

def emit(*args):
    return [
        "\t".join(args)
    ]

def emit_expr_list(l):
    ret = []
    for i, expr in enumerate(l):
        ret+=emit_expr(expr)
        if i!=len(l)-1: ret+=emit("drop", "1")
    return ret

def emit_expr_list_no_value(l):
    ret = []
    for i, expr in enumerate(l):
        ret+=emit_expr(expr)
        ret+=emit("drop", "1")
    return ret

def emit_asm(x):
    r = "\n".join(x[1:])
    return [r]

def emit_label(label):
    return [f"{label}:"]

def emit_load_variable(x):
    if is_fnarg(x):
        return emit_load_fnarg(x)
    return emit("ld", immediate_value(x))

def emit_load_immediate(x):
    if isinstance(x, (int, float)):
        if x < 0 or x > 1023:
            return emit("ldl16", str(x))
        return emit("ldl", str(x))
    elif isinstance(x, str) and constant_lookup(x):
        return emit("ldl", constant_lookup(x)) # + f"\t\t\t;; const {x}")
    elif isinstance(x, str):
        return emit_load_variable(x)
    else:
        print("yarnc ERROR: unknown immediate:", x)

def emit_defvar(x):
    make_variable(x[1])
    return (
        emit_expr(x[2]) +
        emit("st", immediate_value(x[1])) +
        emit("ldl", "0")
    )

def emit_defun(x):
    set_fnarg_mapping(x[1][1:])
    r = (
        emit_label(dash_to_underscore(str(x[1][0]))) +
        emit_expr_list(x[2:]) +
        emit("swap") +
        emit("rts")
    )
    clear_fnarg_mapping()
    return r

def emit_fncall(x):
    result = []
    result.extend(emit("ld", stack_frame, "\t\t;; fncall start"))  # store previous stack location
    # push the arguments on the stack
    t = []
    for arg in reversed(x[1:]): # in reverse order
        t.append(emit_expr(arg))
    result.extend(t)  
    result.extend(emit("sts", stack_frame))  # to find arguments later
    # call the function
    result.extend(emit("jsr", dash_to_underscore(str(x[0]))))
    result.extend(emit("st", working_reg))  # temp store return value
    # remove arguments from the stack
    result.extend(emit("drop", str(len(x[1:]))))
    result.extend(emit("st", stack_frame))  # reinstate previous stack frame
    result.extend(emit("ld", working_reg, "\t\t;; fncall end"))  # load return value
    return result

def emit_set(x):
    if is_fnarg(x[1]):
        return (emit_expr(x[2]) +
                emit("ld", stack_frame) +
                emit("addl", fnarg_lookup(x[1])) +
                emit("stsi") +
                emit("ldl", "0"))
    else:
        return (emit_expr(x[2]) +
                emit("st", immediate_value(x[1])) +
                emit("ldl", "0"))

def emit_forever(x):
    label_start = generate_label("forever_start")
    return (emit_label(label_start) +
            emit_expr_list_no_value(x[1:]) +
            emit("jmp", label_start))

def emit_if(x):
    false_label = generate_label("if_false")
    end_label = generate_label("if_end")
    return (emit_expr(x[1]) +
            emit("jpz", false_label) +
            emit_expr(x[2]) +  # true block
            emit("jmp", end_label) +
            emit_label(false_label) +
            emit_expr(x[3]) +  # false block
            emit_label(end_label))

def emit_when(x):
    end_label = generate_label("when_end")
    false_label = generate_label("when_false")
    return (emit_expr(x[1]) +
            emit("jpz", false_label) +
            emit_expr_list(x[2:]) +  # true block
            emit("jmp", end_label) +
            emit_label(false_label) +
            emit("ldl", "0") +
            emit_label(end_label))

def emit_while(x):
    loop_label = generate_label("while_loop")
    pred_label = generate_label("while_loop_pred")
    return (emit("jmp", pred_label) +  # check first
            emit_label(loop_label) +
            emit_expr_list(x[2:]) +  # loop block
            emit("drop", "1") +  # discard the last expression value
            emit_label(pred_label) +
            emit_expr(x[1]) +  # predicate
            emit("notl") +
            emit("jpz", loop_label) +
            emit("ldl", "0"))

def emit_left_shift(x):
    return emit_expr(x[1]) + emit("rl", immediate_value(x[2]))

def emit_right_shift(x):
    return emit_expr(x[1]) + emit("rr", immediate_value(x[2]))

def unary_procedure(proc, x):
    return emit_expr(x[1]) + emit(proc)

def binary_procedure(proc, x):
    return emit_expr(x[2]) + emit_expr(x[1]) + emit(proc)

def emit_procedure(x):
    if x[0] == 'init': return emit("")
    elif x[0] == 'asm': return emit_asm(x)
    elif x[0] == 'byte': return [f".byte {x[1]}\n"]
    elif x[0] == 'text':
        return [f'.byte "{x[1]}"\n']
    elif x[0] == 'defvar':
        return emit_defvar(x)
    elif x[0] == 'defun':
        return emit_defun(x)
    elif x[0] == 'defconst':
        make_constant(x[1], x[2])
        return []
    elif x[0] == 'set!':
        return emit_set(x)
    elif x[0] == 'if':
        return emit_if(x)
    elif x[0] == 'when':
        return emit_when(x)
    elif x[0] == 'while':
        return emit_while(x)
    elif x[0] == 'do':
        return emit_expr_list(x[1:])
    elif x[0] == 'forever':
        return emit_forever(x)
    elif x[0] == 'eq?':
        return binary_procedure("equ", x)
    elif x[0] == '<':
        return binary_procedure("lt", x)
    elif x[0] == '<=':
        return binary_procedure("lte", x)
    elif x[0] == '>':
        return binary_procedure("gt", x)
    elif x[0] == '>=':
        return binary_procedure("gte", x)
    elif x[0] == 's<':
        return binary_procedure("slt", x)
    elif x[0] == 's<=':
        return binary_procedure("slte", x)
    elif x[0] == 's>':
        return binary_procedure("sgt", x)
    elif x[0] == 's>=':
        return binary_procedure("sgte", x)
    elif x[0] == '+':
        return binary_procedure("add", x)
    elif x[0] == '-':
        return binary_procedure("sub", x)
    elif x[0] == '*':
        return binary_procedure("mul", x)
    elif x[0] == '/':
        return binary_procedure("div", x)
    elif x[0] == 'mod':
        return binary_procedure("mod", x)
    elif x[0] == 'and':
        return binary_procedure("and", x)
    elif x[0] == 'or':
        return binary_procedure("or", x)
    elif x[0] == 'xor':
        return binary_procedure("xor", x)
    elif x[0] == 'bitwise-not':
        return unary_procedure("not", x)
    elif x[0] == 'not':
        return unary_procedure("notl", x)
    elif x[0] == '<<':
        return emit_left_shift(x)
    elif x[0] == '>>':
        return emit_right_shift(x)
    else:
        return emit_fncall(x)

def reset_compiler():
    global label_id, variables, histogram, constants
    label_id = 99
    variables = []
    histogram = []
    constants = constants_table  # Assuming constants_table is defined elsewhere

def add_histogram(name, count, hist):
    for item in hist:
        if item[0] == name:
            return [[name, item[1] + count]] + hist[1:]
    return [[name, count]] + hist

def histogram_print(histogram):
    for h in histogram:
        print(f"{h[0]}: {h[1]}")

def emit_expr(x):
    if immediate(x):
        return emit_load_immediate(x)
    elif primcall(x):
        r = emit_procedure(x)
        global histogram
        histogram = add_histogram(x[0], len(r), histogram)
        return r
    else:
        print(f"yarnc: I don't understand {x}")
        return []

def preprocess_cond_to_if(x):
    def _(l):
        if not l:
            return 0
        elif pre_process(l[0][0]) == 'else':
            return ['do'] + pre_process(l[0][1:])
        else:
            return ['if', pre_process(l[0][0]),
                    ['do'] + pre_process(l[0][1:]),
                    _(l[1:])]
    return _(x[1:])

def preprocess_zero(x):
    return ['eq?', 0, pre_process(x[1])]

def preprocess_led(x):
    return ['set!', 'reg_led', x[1]]

def preprocess_comp_delta_reset(x):
    return ['set!', 'reg_comp_delta_reset', 1]

def preprocess_step_count_reset(x):
    return ['set!', 'step_count_reset', 1]

def preprocess_set_next_move(x):
    return ['set!', 'reg_servo_next_pattern_id', x[1]]

def pre_process(s):
    if not s:
        return s
    elif isinstance(s, list):
        return [pre_process(i) if isinstance(i, list) and i else
                preprocess_cond_to_if(i) if isinstance(i, list) and i and i[0] == 'cond' else
                pre_process(i) for i in s]
    else:
        return s

def compile_program(x):
    global variables
    variables = []    
    s = emit_expr_list(pre_process(x))
    return "\n".join(s)

