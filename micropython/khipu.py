# Copyright (C) 2024 Then Try This
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import utime as time
from machine import I2C, Pin, RTC, SoftI2C

REG_VERSION = 0
REG_SANITY = 1
REG_ID = 2
REG_BROADCAST_ID = 3     # which pendant to write to
REG_BROADCAST_ADDR = 4   # what address to write to
REG_BROADCAST_DATA = 5   # what to write
REG_TRIGGER_SCAN = 6     # set this to 1 then wait for it to clear before reading data
REG_SCAN_DATA_START = 7  # where the structure data will be read

SANITY_CODE = 99
PENDANT_I2C_DEVICE = 20

STATE_LED = 0
STATE_HAPTIC_DURATION = 1
STATE_HAPTIC_PATTERN = 2
STATE_HAPTIC_REPEATS = 3

I2C_DELAY = 0.001

####################################################################

def parse(d,pos,depth,order,container,ids_present):
    pid = d[pos]
    if pid==0: return 1
    if pid not in ids_present:
        ids_present.append(pid)
        
    pend = {
        'id': d[pos],
        'order': order,
        'desc': []
    }

    size = d[pos+1]
    p = 2
    order=0
    while p<size:        
        p+=parse(d,pos+p,depth+1,order,pend['desc'],ids_present)
        order+=1
    container.append(pend)
    return p

def calc_width(struct):
    w = 0
    if len(struct['desc'])>0:
        for dec in struct['desc']:
            w+=calc_width(dec)
        return w
    else:
        return 2
    
def render_struct(struct,display,x,y):
    display.plot(x,7-y,"#")
    y+=2
    oldx=x
    for i,desc in enumerate(struct['desc']):
        render_struct(desc,display,x,y)
        x+=calc_width(desc)
        if len(struct['desc'])==1:
            display.plot(oldx,7-(y-1),"#")
            
        if i<len(struct['desc'])-1:
            for xx in range(oldx,x+1):
                display.plot(xx,7-(y-1),"#")
        
        oldx=x

####################################################################

def read_int(i2c,addr):
    return int.from_bytes(i2c.readfrom_mem(PENDANT_I2C_DEVICE, addr, 1),"little")

def read_pendant(i2c, ids_present):
    #v = i2c.readfrom_mem(PENDANT_I2C_DEVICE, REG_VERSION, 1)
    #print("version: "+str(v))
    # trigger a scan and wait for it to complete
    i2c.writeto_mem(PENDANT_I2C_DEVICE, REG_TRIGGER_SCAN, b'\x00')
    time.sleep(I2C_DELAY)
    scanning = i2c.readfrom_mem(PENDANT_I2C_DEVICE, REG_TRIGGER_SCAN, 1)
    time.sleep(I2C_DELAY)
    while scanning==b'\x01':
        scanning = i2c.readfrom_mem(PENDANT_I2C_DEVICE, REG_TRIGGER_SCAN, 1)
        time.sleep(I2C_DELAY)
    pid = read_int(i2c,REG_SCAN_DATA_START)
    time.sleep(I2C_DELAY)
    size = read_int(i2c,REG_SCAN_DATA_START+1)
    time.sleep(I2C_DELAY)
    data = [pid,size]
    if size>2:
        for pos in range(0,size-2):
            data.append(read_int(i2c,REG_SCAN_DATA_START+pos+2))
            time.sleep(I2C_DELAY)
        # parse the bytes into an array of dicts
        struct=[]
        parse(data,0,0,0,struct,ids_present)
        return struct
    else: return False

def broadcast_cmd(i2c,pid,addr,data):
    i2c.writeto_mem(PENDANT_I2C_DEVICE, REG_BROADCAST_ID, pid.to_bytes(1,"little"))
    time.sleep(I2C_DELAY)
    i2c.writeto_mem(PENDANT_I2C_DEVICE, REG_BROADCAST_ADDR, addr.to_bytes(1,"little"))
    time.sleep(I2C_DELAY)
    i2c.writeto_mem(PENDANT_I2C_DEVICE, REG_BROADCAST_DATA, data.to_bytes(1,"little"))
    time.sleep(I2C_DELAY)
    
##################################################################

def shake_pid(i2c, pid):
    # send repeats first as bug in pendants repeatedly sends last command
    # to subpendants, if we send this last the shaking never stops!
    broadcast_cmd(i2c,pid,STATE_HAPTIC_REPEATS, 1)
    read_pendant(i2c,[]);
    broadcast_cmd(i2c,pid,STATE_HAPTIC_PATTERN, 101)
    read_pendant(i2c,[]);
    broadcast_cmd(i2c,pid,STATE_HAPTIC_DURATION, 30)
    read_pendant(i2c,[]);
    # uncomment for testing purposes...
    #broadcast_cmd(i2c,pid,STATE_LED, 1)
    #read_pendant(i2c,[]);

def shake(i2c, last_ids_present, ids_present):
    for pid in ids_present:
        if pid not in last_ids_present:
            shake_pid(i2c, pid)            
            shake_pid(i2c, pid)
            
def read_khipu(i2c, ids_present):
    c = i2c.readfrom_mem(PENDANT_I2C_DEVICE, REG_SANITY, 1)
    time.sleep(I2C_DELAY)
    if int.from_bytes(c,"little")==SANITY_CODE:
         return read_pendant(i2c, ids_present)
    else: return False
