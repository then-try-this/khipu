import "./App.css";
import Barchart from "./components/Barchart";
import KhipuTree from "./components/Tree";
// import ConnectButton from "./components/ReceiveKhipu";

import { useState } from "react";

// import SerialLoader from "./components/SerialLoader.tsx";
import SerialProvider from "./components/SerialProvider.tsx";
import SerialLoader from "./components/SerialLoader.tsx";

function App() {
  return (
    <SerialProvider>
      <div className="App">
        <h1>Hello.</h1>
        <strudel-editor id="repl"></strudel-editor>
        <SerialLoader>
          <KhipuTree />
        </SerialLoader>
      </div>
    </SerialProvider>
  );
}

export default App;
