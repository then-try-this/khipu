import { useState, useEffect, useRef } from "react";
import { SerialMessage, useSerial } from "./SerialProvider.tsx";

export default function ConnectButton() {
  const { canUseSerial, portState, hasTriedAutoconnect, connect, subscribe } =
    useSerial();

  const connectButtonRef = useRef < HTMLButtonElement > null;

  const onConnectButtonClick = async () => {
    const hasConnected = await connect();
    if (!hasConnected) {
      connectButtonRef.current?.focus();
    }
  };

  useEffect(() => {
    const unsubscribe = subscribe((message) => {
      console.log(`${message.timestamp}: ${JSON.stringify(message.value)}`);
    });
    return () => {
      unsubscribe();
    };
  }, [subscribe]);

  return <button onClick={onConnectButtonClick}>Connect</button>;
}
