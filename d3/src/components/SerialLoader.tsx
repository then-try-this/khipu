import { Fragment, PropsWithChildren, useRef } from "react";
import { useSerial } from "./SerialProvider.tsx";
import React from "react";

interface SerialLoaderProps {}

const SerialLoader = ({ children }: PropsWithChildren<SerialLoaderProps>) => {
  const { canUseSerial, portState, hasTriedAutoconnect, connect } = useSerial();

  const pairButtonRef = useRef<HTMLButtonElement>(null);

  const onPairButtonClick = async () => {
    const hasConnected = await connect();
    if (!hasConnected) {
      pairButtonRef.current?.focus();
    }
  };

  // If can't use serial, return error message
  if (!canUseSerial) {
    return (
      <div>
        <p>Your browser doesn't support webserial.</p>
        <p>Please try switching to a supported browser (e.g., Chrome 89).</p>
      </div>
    );
  }

  // If port is open, show the children!
  if (portState === "open") {
    return <Fragment>{children}</Fragment>;
  }

  // If autoconnect hasn't run its course yet, wait for that...
  if (!hasTriedAutoconnect) {
    return null;
  }

  // If autoconnect fails, then show manual connect button

  let buttonText = "";
  if (portState === "closed") {
    buttonText = "Pair device";
  } else if (portState === "opening") {
    buttonText = "Pairing...";
  } else if (portState === "closing") {
    buttonText = "Disconnecting...";
  }

  return (
    <div>
      <div>
        <p>
          Connect your khipu via USB and pair with your browser to get started.
        </p>

        <button
          ref={pairButtonRef}
          disabled={portState === "opening" || portState === "closing"}
          onClick={onPairButtonClick}
        >
          {buttonText}
        </button>
      </div>
    </div>
  );
};

export default SerialLoader;
