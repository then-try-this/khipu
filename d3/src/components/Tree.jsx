import React, { useEffect, useState } from "react";
import Tree from "react-d3-tree";
import { useSerial } from "./SerialProvider.tsx";

const strands = {
  23: "function",
  26: 1,
  8:  2,
  6: 2,
  25: 3,
  3: 5,
  21: 6,
  22: 7,
  1: "function",
  17: "function",
  2: "function",
  19: "function",
  24: "function",
};

const solkattu = {
  1: '1',
  2: '1 2',
  3: '1 2 3',
  4: '1 2 3 4',
  5: '1 2 3 4 5',
  6: '1 2 3 4 5 6',
  7: '1 2 3 4 5 6 7',
  8: '1 2 3 4 5 6 7 8',
  9: '1 2 3 4 5 6 7 8 9'
}

const preamble = `const toPhrase = p => p.fmap(run).stepJoin();
  const yati = (n, ...pats) => {
  const pat = s_cat(...pats.map(toPhrase)); 
  return s_taper(n, pat.tactus, pat);
                             }
const teermana = (n, ...pats) => s_expand(n, ...pats.map(toPhrase));
`;

const functions = {1: 'cat', 2: 'teermana', 3: 'yati'};

function parseTree(tree) {
  const khipu = { name: "khipu", children: [] };
  const children = [];

  for (let i = 0; i < 4; ++i) {
    let child;
    if (tree[i]) {
      child = parseBranch(tree[i][0], 0);
      //   console.log(child);
    } else {
      child = { name: "", children: [] };
    }
    children.push(child);
  }
  if (children.length > 0) {
    // khipu.children = [{ name: "pendant", children: children }];
    khipu.children = children;
  }
  return khipu;
}

function parseBranch(tree, depth) {
  const name = tree.id in strands ? strands[tree.id] : "<" + tree.id + ">";
  const top = { name, children: [] };
  const children = [];
  if (tree.desc?.length) {
    const maxslot = Math.max(...tree.desc.map((x) => x.order));
    for (let i = 0; i <= maxslot; ++i) {
      const [childtree] = tree.desc.filter((x) => x.order === i);
      let child;
      if (childtree) {
        child = parseBranch(childtree, depth + 1);
      } else {
        child = { name: "", children: [] };
      }
      children.push(child);
    }
  }
  if (children.length > 0) {
    // top.children = [{ name: "pendant", children: children }];
    top.children =  children;
  }
  //   top.children.push({ name: "<end>" });
  return top;
}

function toStrudel(tree) {
  switch (tree.name) {
    case "khipu":
    case "cat":
      if (tree.children[0]?.name == "pendant") {
        return (
          "s_cat(" + tree.children[0].children.map(toStrudel).join(", ") + ")"
        );
      }
    case "s_expand":
      let args = tree.children[0]?.children || [];
      args = args.map(toStrudel).filter((x) => x != "nothing");
      // .join(", ");
      //   console.log(args, args.length);
      const pat = args.pop();
      const arg = args.join(", ");
      return pat + ".s_expand(" + arg + ")";
    case "":
      return "nothing";
    default:
      return tree.name;
  }
}

function toKonnakolPattern(pendant) {
  if (pendant.name === 'function') {
    const function_id = pendant.children[0]?.name;
    if (function_id) {
      const func = functions[function_id];
      const pats = [];
      for (let i = 1; i < pendant.children.length; ++i) {
        if (pendant.children[i].name) {
          pats.push(toKonnakolPattern(pendant.children[i]));
        }
      }

      switch(func) {
        case 'yati':
        case 'teermana':
          const param = pats.shift();
          return func + '(' + param + ', s_cat(' + pats.join(',') + '))';
        default:
          return 's_cat(' + pats.join(',');
      }
    }
  }
  else {
    return(pendant.name); 
  }
}

function toKonnakol(pendant) {
  const inputs = 3
  const tala = pendant.children[0].name || 8;
  // console.log('tala', tala);
  const patterns = []
  for (let i = 1; i <= inputs; ++ i) {
    const name = pendant.children[i].name;
    if (name) {
      patterns.push(toKonnakolPattern(pendant.children[i]));
    }
  }
  return('serial(s_cat(' + patterns.join(',') + ').steps(' + tala + ').fmap(x => x + \':50\\n\'))')
//
}

export default function KhipuTree() {
  const { subscribe } = useSerial();
  const [serialBuffer, setSerialBuffer] = useState("");
  const [started, setStarted] = useState(false);

  const initialTree = {
    name: "khipu",
    children: [],
  };

  const [khipuTree, setKhipuTree] = useState(initialTree);
  const [strudelPattern, setStrudelPattern] = useState("silence");

  useEffect(() => {
    const unsubscribe = subscribe((message) => {
      // console.log(message);
      setSerialBuffer(function (value) {
        value = value + message.value;
        const match = /^(.*?)\r\n(.*)/.exec(value);
        if (match) {
          try {
            const json = match[1]
              .replace(/'/g, '"')
              .replace(/True/g, "true")
              .replace(/False/g, "false")
              .replace(/(\d+):/g, '"$1":');
            const newTree = parseTree(JSON.parse(json));
            // console.log(newTree);
            setKhipuTree(newTree);
            // console.log(newTree)
            // setStrudelPattern(toStrudel(newTree));
            const code = preamble + "\n\n" + toKonnakol(newTree);
            setStrudelPattern(code);
            // console.log('code', code)
            const editor = document.getElementById("repl").editor;
            if (started && editor.code !== code) {
              editor.setCode(code);
              editor.evaluate();
            }
          } catch (e) {
            console.error(e);
          }
          return match[2];
        }
        return value;
      });
    });
    return () => {
      unsubscribe();
    };
  }, [subscribe, started]);
  let button;
  if (started) {
    button = <button onClick={() => setStarted(false)}>stop</button>;
  }
  else {
    button = <button onClick={() => {setStarted(true);}}>start</button>;
  }
  return (
    // `<Tree />` will fill width/height of its container; in this case `#treeWrapper`.
    <div id="treeWrapper" style={{ width: "75em", height: "50em" }}>
      <div>{button}</div>
      {strudelPattern}
      <Tree data={khipuTree} orientation="vertical" collapsible={false} />
    </div>
  );
}
