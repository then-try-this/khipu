import serial
import time
import json
import pygame

pygame.init()
screen = pygame.display.set_mode([2000,1000])
font = pygame.font.SysFont('Calibri', 50, True, False)

s = serial.Serial("/dev/ttyACM0", 115200)

id_to_knot = {
    1: "-/-",
    2: "-/--/-",
    3: "-//-/-/-",
    4: "-//--/-",
    5: "-/-/-/-",
    6: "-///-",
    9: "-////-",
    16: "-/--//-",
    17: "-/--///-",
    18: "-//-",
    19: "-//-",
}


# {'desc': [{'desc': [], 'id': 5, 'order': 2}], 'id': 1, 'order': 0}

def render_khipu(k):
    for index,k in k.items():
        render_pendant(k,400+index*600,200,400)

        
def render_pendant(k,x,y,w):
    if k['id'] in id_to_knot:
        knots = id_to_knot[k['id']]
    else:
        knots = "?"+str(k['id'])
    text = font.render(knots, True, (0,0,0))
    text = pygame.transform.rotate(text, 90)
    screen.blit(text, [x-25, y])
    num_children = len(k["desc"])
    if num_children>0:
        g = w/num_children
        #off = (g*num_children)/2
        for i,k in enumerate(k["desc"]):
           nx = (x+g*i)
           ny = y+200
           pygame.draw.line(screen, (0,0,0), [x, y+100], [nx, ny], 5)  
           render_pendant(k,nx,ny,g)
    
    #pygame.draw.line(screen, (255,0,0), [x, y], [100, 110], 5)       
 
kl = {}
    
while True:
    if s.in_waiting:
        st = s.readline()
        st = str(st).replace("\'", "\"").rstrip()[2:-5]
        k = json.loads(st)
        kl[k[0]]=k[1]
    
    screen.fill((255,255,255))    
    render_khipu(kl)
    pygame.display.flip()
    time.sleep(0.1)
