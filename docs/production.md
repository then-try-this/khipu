# Mass production 

Notes on making lots of these.

PCB fabricated by [seeed](https://www.seeedstudio.com) - the [gerber files created from KiCad are here](https://gitlab.com/then-try-this/khipu/-/tree/alpaca/hardware/khipu/seeed) (the ones generated [for CNC milling are here](https://gitlab.com/then-try-this/khipu/-/tree/alpaca/hardware/khipu/gerber?ref_type=heads)) 

![](pics/pcbs.jpg)

![](pics/soldering-batch.jpg)

There are a couple of problems with this version I need to fix:
- We need to add a pullup resistor for the outgoing SCL connection. This causes a lockup in the i2c code if it's not there (waiting for SCL to go high), luckily it's fairly easy to bodge one on the back side of the board. This needs to be fairly high (100kΩ) as it will be in parallel to the ones on downward connected pendants. (TODO: I think we could remove these? Also with the shared SCL they will all be parallel to each other so with four connected: ~1.16KΩ which might be a problem)
- The strain relief holes for the cables could do with being a little larger, not sure if this is possible without compromising on strength of the board overall however.
- This was designed with clearance for CNC cutting and single sided construction - it should be possible to shrink this form factor *a lot*. Not quite sure if this is needed though.

For soldering the plugs onto the boards, using 0.4mm copper enamelled wire is much easier than small sections of insulated wire.

Manufacture of the four 3.5mm TRRS (tip, ring, ring, sleeve) sockets and cables (and soldering them to the board) is quite a considerable time sink. It turns out buying these premade is much easier - and also much more robust than the kind you buy for hand soldering, partly as they are properly attached to the cables for strain relief. This will mean they can cope with a lot more rough handling (which is expected!).

