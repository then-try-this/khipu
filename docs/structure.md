# Structural representation(s)

At the moment the khipu structures are described in two forms, one a low level bytecode that is used by the attiny microcontrollers to concatenate together their descriptions across the heirachical network. They capture the descriptions from their child pendants and enclose them in their own data structures.

These are then read by the Raspberry Pi Pico and converted into more convenient nested dictionaries. These in future should probably be further converted into JSON for more easily using with other devices.

## High level

Within the micropython code we parse the low level bytes described below and convert them into a recursive dictionary:

    {
        'id': 9,
        'order': 2, # the order we are in in our parent
        'desc': [], # a list of more sub-pendants and their structures
    }

The reason for the order parameter is that we need to be able to deal with 'absent' pendant - pendants may be e.g. plugged into only the first and third sockets.

So a single empty pendant would be:

    { 'id': 3, 'order':0, [] }

The four pendant example from below:

    { 
        'id': 1, 
        'order': 0,
        'desc': [{
                    'id': 2,
                    'order': 0,
                    'desc': [{
                        'id: 3,
                        'order': 0,
                        'desc': []
                    },
                    {
                         'id':4
                         'order': 1,
                         'desc': []
                    }]
                ]}
        ]
    }
        
We use these nested dictionaries to render a schematic view of the khiup structure on an 8x8 LED matrix: 

![](pics/led.jpg)

## Low level

The microcontrollers in the khipu hardware need to be able to read and concatenate their structures in a hierarchical manner. To do this using their nested i2c networks, they need to be able to describe everything in a simple list of byte data. This has changed somewhat from that described in the inital planning. This is the representation of an empty pendant:

    [id size 0 0 0 0] 

The size is inclusive of id and size itself, so a valid example would be:

    [34 6 0 0 0 0] 

All empty khipu are 6 bytes long. The ids can be any non-zero number. The zeros in the last 4 bytes indicate all four sockets are empty (this allows for arbitrary numbers of sockets). 

A real example with four pendants connected together:

    [1 21 2 16 3 4 0 0 0 0 4 4 0 0 0 0 0 0 0 0 0]

The sizes allow for these byte strings to be split up into the nested heirachy. This is easier to see if we break it down:

    [1 21 [2 16 [3 6 0 0 0 0] [4 6 0 0 0 0] 0 0] 0 0 0]]

### Problems

The values are bytes for the moment, but should probably be 16 bit shorts as at some point we'll have a structure larger than 256 values ('unlikely' that we'll need more than this as IDs?). 

We currently have a hard limit on the length of the structures due to the underlying i2c addresses being bytes - however we should be able to get around this by paging.

It should also be possible to compress this description a bit using a special code for an empty pendant, rather than listing the 'null' zero ids.
