# Second phase research notes 
Sheffield 19/03/24

Khipu pendants working on a technical level, making sound via midi and robots move simply.

## Research:

Starting off with some reading - the papers below [archived in the repo here](https://gitlab.com/then-try-this/khipu/-/tree/alpaca/papers?ref_type=heads)
    
### Knot notation
          
- [Khipu field guide](https://www.khipufieldguide.com) some good (post-Urton) rendering of quipu.
  in "Dumbarton Oaks Research Library and Collection"
- [The application of Inca khipuas an accountability and managerial control tool](https://rbgn.fecap.br/RBGN/article/view/613/pdf) - written by two accountants.
- [The US National Institute of Standards and Technology made a khipu](https://www.nist.gov/nist-museum/standardizing-empire)

### Modern khipu

- [A Modern Khipu from Cutusuma, Bolivia](https://www.iai.spk-berlin.de/nachlass/uhle/pdf/a%20modern%20kipu%20from%20cutsuma,%20bolivia.pdf)
- [Modern Khipu context filmed by Paola Torres Núñez del Prado](https://www.youtube.com/watch?v=Jjc9-xGGR3E)
        
### Wari

- [Wari culture exhibition](https://www.doaks.org/resources/online-exhibits/written-in-knots), including [this image](https://www.doaks.org/resources/online-exhibits/written-in-knots/images/20.jpg) showing an interpretation of a wari quipu. 
- [Splitstoser, Jeffrey. 2020. “Los khipus Wari.” In Khipus, edited by Cecilia Pardo, 28–35. Lima: Museo de Arte de Lima.](https://mali.pe/wp-content/uploads/2020/11/Los-khipus-Wari.pdf)

### Questions:

- How to standardise our way of encoding and describing knots allowing for a variety of uses?
- What can we learn from Wari khipus - how much research has been done into them and where is it?
- How can we respresent higher order things like partial application, if we need them?

### Decisions:

- Trees should be syntax error free, all possible trees should be valid
- Two 'types' operators with 4 ordered descendant sockets (maybe no id knot needed), operands with no sockets.
- Include sequences and transformations.
- Movement and sound perhaps share the same language.
- Program robot servo sequences directly
- Use texture/material for more description and colour (as secondary element) 

## Improved textile/wari based design

A problem with current design is that it's trees look too computer
sciency (balanced & heirachical) and not khipu enough (tribitaries
ordered by meanings):
    
![](https://gitlab.com/then-try-this/khipu/-/raw/alpaca/pics/wari-vs-compsci.jpg)

A solution would be to arrange descendant sockets into rows, this also
incorporates the fabric better into the sockets. We can wrap (a common
method used in Wari khipu) the cables - this is useful to secure them.
    
![](https://gitlab.com/then-try-this/khipu/-/raw/alpaca/pics/newwrap.jpg)

The operands/leaves/terminals can be simplified to being perhaps the
only ones with knots.

Other notes:
  
- Minimising knots needed for support, e.g. do we need them on the
  ends - or "embrace the fraying?"

- Hitching, in many ways the plugs and sockets are a discontinuity
  with the textile flowing through the khipu. They replace the need
  for hitching - but we can still use the term (especially irt people
  in workshops having done khipu making previously this will make
  sense) so connecting = hitching.
    
## Chat with Paola

Discussion on politics of machines conference talk and what we want to show.

Our methodology is the most important aspect of our work (above making
devices as 'end products') which we need to focus on explaining.

- Learning through doing, gaining unwritten, tacit knowledge by making
  things. Making things rather than writing about them or collecting
  them.
    
- We want to make a _communal_ method of computing in a _social context_, 
  we need to study this aspect particularly and have records of interactions 
  in workshops as (perhaps the main) part of our output.

- Mimicry vs appropriateness: avoiding aesthetic appropriation that
  doesn't add or contribute to the design in a practical way.

- Being able to refer to contexts/cultures in a fun but respectful
  way: e.g. poking fun at the hundreds of plugs, wires and sockets used 
  with modular synthesisers

- Using bright colours and not sticking to khipu palette is good
  (or at least doesn't need to be a restriction) if it's to be used
  with kids or VI people.

- Being able to show at some point that our solutions are good for
  something in these terms (better than other methods).

## Design changes

Prototyping the new cable wrap and plyed pendant:

![](https://gitlab.com/then-try-this/khipu/-/raw/alpaca/pics/proto.jpg)

Finding a new material - given limited choices we went with 3mm 100% recycled cotton macramé in "strangely beige" and "wicked white" as the only other colour was black (which doesn't seem to appear in khipus):

![](https://gitlab.com/then-try-this/khipu/-/raw/alpaca/pics/material.jpg)

Day three was a day of making and twisting with this material, replacing our previous colourful but not really up to scratch magician's rope. Making our own cord (and finding space to do it) is an significant part of the process:

![](https://gitlab.com/then-try-this/khipu/-/raw/alpaca/pics/twist.jpg)

![](https://gitlab.com/then-try-this/khipu/-/raw/alpaca/pics/manufacture.jpg)

![](https://gitlab.com/then-try-this/khipu/-/raw/alpaca/pics/one.jpg)

![](https://gitlab.com/then-try-this/khipu/-/raw/alpaca/pics/knotted.jpg)

Our handmade rope unfortunately doesn't show the knots very well, we'll look for smoother cord of the same material. Also concerns about white being a bad choice to keep them clean, but at least the knot cords can be easily detached and washed. Neutral colours seem a good idea at this point, as we can add our colourful wrapping on top to denote different meanings and change them as we like. 

Putting them together, the new wrapped "operators" give a much clearer structure as they keep the descendants better spaced, and critically the whole thing has much more of a textile feel to it:

![](https://gitlab.com/then-try-this/khipu/-/raw/alpaca/pics/far.jpg)

![](https://gitlab.com/then-try-this/khipu/-/raw/alpaca/pics/close.jpg)

# Strudel

With some tips from [Felix Roos](https://github.com/felixroos), we
managed to get our khipu working with the
[strudel](https://strudel.cc/) live coding environment, [hacking
something together](../strudel/test.html) to map pendants to functions
and values. This was gratifyingly easy, just a matter of writing a
HTML file with some javascript to imported strudel, render an editor,
transpile the data read via webserial into javascript, and updating
the code with the results and triggering an evaluation. We were short
on time and energy at this point, but still it was great to already be
able to make and transform sequences by 'hitching' (well, plugging)
the strands together. A nice proof-of-concept that we forgot to take a
video recording of.

# Todos

- Make video demo showing them working and explaining some of the above points.
- Convert output to actual JSON rather than conveniently similar but not quite Python dict strings

# Continuing work

Firstly get them working with robots for demo video - sequencing the legs with knots:

- We have too many branches and not enough leaves.
- Using three branches, one each for front/middle/back servos
- Different knots for different angles (ascii codes are a, b, c, d, A, B, C, D and 0)
- Walking is AAaa AaaA AAaa
- Use empty as repeat last could be A-a- Aa-A A-a-

Converting to knots, where -//- is "A" and -/- is "a":

    ----O---------O--------O-------
        |         |        |
        o-//-     o-//-    o-//-
        |         |        |
        o         o-/-     o
        |         |        |
        o-/-      o        o-/-
        |         |        |
        o         o-//-    o
        |         |        | 

Requires 3 branches and 7 leaves

Bought new 8mm hemp rope for the knots, much better at showing the knots and can be untwined and looped through for hitching.

# More complicated robot behaviour

Thinking about a knot version of the yarn blocks language - we
currently have a complete language stack:

* Yarn blocks visual code, which is converted into:
* Yarn lisp, which gets compiled to:
* Yarn assembly which gets converted into:
* Yarn bytecode [used in radio packets and written directly to robot VM]

These (all except yarn blocks) are all now running entirely on the
Raspberry Pi Pico in micropython.

Conceptually it might be nice to go from knots directly to bytecode but...

Lets try lisp first.

There are lots of registers set using e.g.:

    (set! servo-2-bias -10)

Usual lisp language features, conditionals: `cond`, `if`, `when`,
functions: `defun`, variables `defvar` and arithmetic.

Problem with a 'straight' conversion where each branch is a lisp
expression is that khipu themselves seem to lend themselves to much
more minimal forms, e.g. branches only having one meaning etc.

Could use a sequencer approach like Alex - so wiggling legs can be
controlled heirachically where speed = depth. Maybe something in the
middle? Maybe something 'interpreted' rather than one-to-one mapping
to code?

Simplest is probably where knots are distinct actions sequenced
linearly. Easiest to grasp, uses nothing plugged in as meaning and
expands limitation of four sub-pendants in an interesting way.

    nothing  : wait one 'step'
    -/-      : walk forward one step
    -//-     : walk backward one step
    -///-    : turn left one step
    -////-   : turn right one step
    -/-/-    : led flash once
    -/-//-   : led flash twice
    -//-/-   : speed slow
    -//-//-  : speed normal
    -//-///- : speed fastest
    

      Robot  A            B
      -------O------------O--- ...
             |
           1 o----. 
             |    |
           6 o  2 o-//-
             |    |
           7 o-/- o 3
             |    |
           8 o  4 o
                  |
                5 o-/-//-
                                
A depth first 'search' so robot A:

1. Waits
2. Forwards one step
3. Waits
4. Waits
5. Flashes LED
6. Waits
7. Back one step
8. Waits

Interesting enough, and in combination with other robots will be a
kind of choregraphy. *NOTE* default to send to all robots so they all
follow the same movements until you start adding more pendants.

Each knot/pendant vibrates when it's active (is that possible?). Maybe
if we loosely do it with timing and re-sync at the start of each loop,
syncing used to work really well so....

Each knot is represented by a code block, much like blocky - so, wait
(no knot) is:

    (set! usr-d 40)
    (while (> usr-d 0) 
        (set! usr-d (- usr-d 1))) 

Similarly, flash LED once (-/-/-) looks like:

    (set! led 1)
    (set! usr-d 40)
    (while (> usr-d 0) 
        (set! usr-d (- usr-d 1))) 
    (set! led 0)
    
Walk forwards one step (-/-):

    (set! next-pattern walk-forwards)
	(set! step-count-reset 1)
	(while (< step-count 1) 0)
	(set! next-pattern walk-stop)
    
Pendant JSON to sequence:

    pend = {
        'id': 20,
        'order': 1,
        'desc': [
            {
                'id': 24,
                'order': 0,
                'desc': []
            },
            {
                'id': 25,
                'order': 2,
                'desc': [
                    {
                        'id': 30,
                        'order': 0,
                        'desc': []
                    }
                ]
            },
            {
                'id': 21,
                'order': 3,
                'desc': []
            }
        ]
    }

Becomes (a list of ids and 0 as no-knot): 

    [0, 24, 0, 30, 0, 0, 0, 0, 21, 0, 0, 0]

Expands to this:

    [['set!', 'usr-d', 40], 
     ['while', ['>', 'usr-d', 0], 
       ['set!', 'usr-d', ['-', 'usr-d', '1']]], 
     ['set!', 'next-pattern', 'walk_backwards'], 
     ['set!', 'step-count-reset', 1], 
     ['while', ['<', 'step-count', 1], 0], 
     ['set!', 'next-pattern', 'walk-stop'], 
     ['set!', 'usr-d', 40], 
     ['while', ['>', 'usr-d', 0], 
       ['set!', 'usr-d', ['-', 'usr-d', '1']]], 
     ['set!', 'led', 1], 
     ['set!', 'usr-d', 40], 
     ['while', ['>', 'usr-d', 0], 
       ['set!', 'usr-d', ['-', 'usr-d', 1]]], 
     ['set!', 'led', 0], ['set!', 'usr-d', 40], 
     ['while', ['>', 'usr-d', 0], 
       ['set!', 'usr-d', ['-', 'usr-d', '1']]], 
     ['set!', 'usr-d', 40], 
     ['while', ['>', 'usr-d', 0], 
       ['set!', 'usr-d', ['-', 'usr-d', '1']]], 
     ['set!', 'usr-d', 40], 
     ['while', ['>', 'usr-d', 0], 
       ['set!', 'usr-d', ['-', 'usr-d', '1']]], 
     ['set!', 'usr-d', 40], 
     ['while', ['>', 'usr-d', 0], 
       ['set!', 'usr-d', ['-', 'usr-d', '1']]], 
     ['set!', 'next-pattern', 'walk_forwards'], 
     ['set!', 'step-count-reset', 1], 
     ['while', ['<', 'step-count', 1], 0], 
     ['set!', 'next-pattern', 'walk-stop'], 
     ['set!', 'usr-d', 40], 
     ['while', ['>', 'usr-d', 0], 
       ['set!', 'usr-d', ['-', 'usr-d', '1']]], 
     ['set!', 'usr-d', 40], 
     ['while', ['>', 'usr-d', 0], 
       ['set!', 'usr-d', ['-', 'usr-d', '1']]], 
     ['set!', 'usr-d', 40], 
     ['while', ['>', 'usr-d', 0], 
       ['set!', 'usr-d', ['-', 'usr-d', '1']]]
    ]

Maybe we should use some functions! Or even better to use a resident
yarn program in eeprom to interpret the string which is directly
written to memory. For the moment this provides the most flexibility
for code generation though, and tests out things well.

It compiles to:


