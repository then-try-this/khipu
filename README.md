A tangible interface that allows construction and manipulation of hierarchical tree structures, culturally appropriated (see below) from the Khipu (records of information digitally encoded in knots) belonging to the Andean civilisation.

As some of the people working on this project are europeans, we acknowledge our cultural appropriation of a historically marginalised and exploited people. Khipu are not our "discovery" or our "artistic inspiration" but a hope for a better world. See also [this blog post](https://thentrythis.org/notes/2023/06/20/working-with-khipu-andean-culture/)

**Why?** We see the [limitations of our predominant thinking aids (i.e. computational technology)](http://worrydream.com/TheHumaneRepresentationOfThought/note.html) as increasingly holding back our society from good decision making, by relegating different forms of experience to "secondary status". In order to counter this it is necessary to look for [other societies who also use digital technologies to store, process and manipulate information](https://khipumantes.github.io/).

As with our work on the [Penelope project](https://penelope.hypotheses.org/), we consider the Inca as holders of precious information and practises that have potential to alleviate serious and widespread problems in the predominant sociotechnical situation of today.

This is a prototype that elevates touch and physical manipulation to a higher status in programming, up to now a field considered to exist in a mythical plane of "abstract thought" known as "cyberspace". Perhaps we can use it to explore the ways this strongly restricted, disembodied thinking is in fact making us stupider, overall.

## Documentation

* [Initial design](https://gitlab.com/then-try-this/khipu/-/blob/alpaca/design/design.md)
* [Structural representation](https://gitlab.com/then-try-this/khipu/-/blob/alpaca/docs/structure.md)
* [Mass production](https://gitlab.com/then-try-this/khipu/-/blob/alpaca/docs/production.md)
* [Phase 2 research](https://gitlab.com/then-try-this/khipu/-/blob/alpaca/docs/research.md)

## Running and install

We are using micropython for all the firmware running on a raspberry
pi pico microcontroller.

Thonny is a bit of a pain when it comes to knowing what state
everything is in and where you are running it from. `mpremote` seems
to make the job a little easier.

Open repl:

    mpremote connect /dev/ttyACM0
    
(use ctrl-] to exit)

Copy over code to test:

    mpremote connect /dev/ttyACM0 cp main.py :
    
Exec and see result:
    
    mpremote connect /dev/ttyACM0 exec "print(1)"

Run a file from local and print output:

    mpremote run test.py
    
